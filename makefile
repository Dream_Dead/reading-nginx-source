
NGX_SRC_DIR=nginx-1.14

all:
	echo 'do nothing'

setup:
	git clone https://github.com/nginx/nginx --branch branches/stable-1.14 --single-branch --depth 1 ${NGX_SRC_DIR}
	(cd ${NGX_SRC_DIR}; rm -rf .git)

compile:
	(cd ${NGX_SRC_DIR}; ./auto/configure --with-debug --prefix=/tmp/nginx; make; make install)

# need cscope and GNU global
tags:
	find ${NGX_SRC_DIR} -name *.h -or -name *.c > cscope.files
	cscope -bkq
	gtags -f cscope.files

